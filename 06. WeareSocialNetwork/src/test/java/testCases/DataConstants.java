package testCases;

public class DataConstants {
    public static final String ADMIN_USERNAME = "Penchoadmin";
    public static final String REGULAR_USERNAME = "Pencho";
    public static final String EMAIL = "pencho@bogdanovi.com";
    public static final String PASSWORD = "123456";
    public static final String CONFIRMATION_PASSWORD = "123456";
    public static final String PROFESSION = "Accountant";
    public static final String FIRST_NAME = "Pencho";
    public static final String LAST_NAME = "Penchev";
    public static final String BIRTH_DATE_DATE = "01";
    public static final String BIRTH_DATE_MONTH = "01";
    public static final String BIRTH_DATE_YEAR = "2002";
    public static final String PROFILE_DESCRIPTION = "I love my profession";
    public static final String SERVICE_1 = "Accounting services";
    public static final String SERVICE_2 = "Painting services";
    public static final String SERVICE_3 = "Construction services";
    public static final String SERVICE_4 = "Carrying heavy on away";
    public static final String SERVICE_5 = "Culinary services";

}
