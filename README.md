# Pluto Team

## Table of contents
1. [Project introduction](#project_introduction)
2. [System under test description](#description)
3. [Team](#team)
4. [Test tools](#test_tools)
5. [Deliverables](#deliverables)
6. [Running automation scripts](#running_automation_scripts)

## Project introduction <a name= "project_introduction"></a>
This project is part of our study curriculum in the Telerik Academy Alpha QA track. It is the final project before graduation. In it, we put everything we know into practice to show our progress.


## System under test introduction <a name="description"></a>
The WEare Social Network is a web application that enables people looking for new career opportunities to connect with each other and with employers.

**The system main functionality**
 - to  connect with other people; 
 - to create, comment on and like posts about new jobs
## QA team members <a name="team"></a>
- Viktor Varbanov
- Valentin Bogdanov


## Test tools <a name="test_tools"></a>
- Source code management - GitLab
- Bug tracking tool - GitLab
- Api - Postman
- UI testing  - Java + Selenium WebDriver + POM
- Schedule management tool - Trello board
- Test framework - Junit 5

## Deliverables <a name = "deliverables"></a>
 - <a href="https://e1.pcloud.link/publink/show?code=XZ48KHZ564ITYvLPx7lum5IyY80spHRwan7">Test plan</a>
 - <a href="https://e1.pcloud.link/publink/show?code=XZQ8KHZKeTSMJY85H00VQF3umayoHAnlP87">Test cases</a>
 - <a href="https://e1.pcloud.link/publink/show?code=XZT8KHZlR6DfEpIWsHCodb9L4Akhb8DPXz7">Test report</a>
 - <a href="https://e1.pcloud.link/publink/show?code=XZB8KHZAK9vWKOSmE0Ia9WLvOM3yBPfbhG7">Exploratory Testing Report</a>
 - <a href="https://e1.pcloud.link/publink/show?code=XZn8KHZI3G0JhkCzyfzRgPkEBNhA8FwS5Bk">XMind scheme</a>
 - <a href="https://trello.com/b/I0vw8KQC/final-project-of-the-pluto-group">QA Project Management Board - Trello</a>
 - <a href="https://e1.pcloud.link/publink/show?code=XZj8KHZRoW43WDVkO83gPLy4kdF3jjWuJVV">Final Test Report</a>
  - <a href="https://e1.pcloud.link/publink/show?code=XZC8KHZkgphIHzNzJQtJHdhTPAjmpnKeLFk">Prerequisites for starting the project</a>



## Running automation scripts <a name= "running_automation_scripts"></a>
**Required software and programs:**
- Windows 10 operating system
- JDK 11
- Java 1.8
- Docker 
- Docker Desktop WSL 2 backend
- IntelliJ IDEA Community edition
- Postman - v.8.8.0 or later
- Apache Maven v.3.8.1
- Firefox
- Google Chrome
- Node.js v.14.17.1 or later
- npm v.6.14.13 or later
- Newman v.5.2.4
- Newman HTML Extra Reporter latest version
  
**Execution of the scripts**
- clone the repo on your computer
- run .bat file in each folder
